import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class Pokemon {
  String id;
  String imageUrl;
  String name;
  int nationalPokedexNumber;
  List<String> types;

  Pokemon(this.imageUrl, this.name);

  Pokemon.fromJSON(Map<String, dynamic> card) {
    this.imageUrl = card['imageUrl'];
    this.name = card['name'];
    this.id = card['id'];
    this.nationalPokedexNumber = card['nationalPokedexNumber'];
    // this.types = card['types'];

    downloadFile(this.imageUrl);
  }

  downloadFile(imageUrl) async {
    await DefaultCacheManager().downloadFile(imageUrl);
  }

  @override
  String toString() {
    return 'Pokemon{name: $name, imageUrl: $imageUrl}';
  }
}
