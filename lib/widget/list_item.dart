import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ListItem extends StatefulWidget {
  //final fields
  final String id;
  final String imageUrl;
  final String name;

  ListItem(this.id, this.name, this.imageUrl);

  @override
  _CustomListItemState createState() => _CustomListItemState();
}

class _CustomListItemState extends State<ListItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Text(widget.name),
          CachedNetworkImage(imageUrl: widget.imageUrl)
        ],
      ),
    );
  }
}
