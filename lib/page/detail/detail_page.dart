import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/model/pokemon.dart';

class DetailPage extends StatelessWidget {
  final Pokemon pokemon;

  const DetailPage({Key key, this.pokemon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(pokemon.name),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: [
              CachedNetworkImage(imageUrl: pokemon.imageUrl),
              Text("#" +
                  pokemon.nationalPokedexNumber.toString() +
                  " " +
                  pokemon.name),
            ],
          ),
        ),
      ),
    );
  }
}
