import 'package:flutter/material.dart';
import 'package:pokedex_app/api/pokemon_api.dart';
import 'package:pokedex_app/page/detail/detail_page.dart';
import 'package:pokedex_app/widget/list_item.dart';

class HomePage extends StatefulWidget {
  @override
  _CustomHomePageState createState() => _CustomHomePageState();
}

class _CustomHomePageState extends State {
  List _pokemons;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pokedex"),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _onRefreshPressed,
          )
        ],
      ),
      body: Center(
        child: _bodyDependingOnState(),
      ),
    );
  }

  _bodyDependingOnState() {
    if (_isLoading) {
      return CircularProgressIndicator();
    } else if (_pokemons == null) {
      return Text("Pas encore de pokemon !");
    } else {
      return new ListView.builder(
          itemCount: _pokemons.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return Container(
              margin: const EdgeInsets.all(10.0),
              width: 400.0,
              height: 400.0,
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              DetailPage(pokemon: _pokemons[index]),
                        ),
                      );
                    },
                    child: ListItem(
                      _pokemons[index].id,
                      _pokemons[index].name,
                      _pokemons[index].imageUrl,
                    ),
                  ),
                ],
              ),
            );
          });
    }
  }

  _onRefreshPressed() async {
    setState(() {
      _isLoading = true;
    });
    List pokemons = await PokemonAPI.getPokemons();
    setState(() {
      _pokemons = pokemons;
      _isLoading = false;
    });
  }
}
