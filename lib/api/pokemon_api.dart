import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pokedex_app/model/pokemon.dart';

class PokemonAPI {
  static final String baseUrlPath = "https://api.pokemontcg.io/v1";
  static final String cardsUrlPath = baseUrlPath + "/cards";

  static Future<List> getPokemons() async {
    List pokemons = [];
    var response = await http.get(
      cardsUrlPath,
    );
    var json = jsonDecode(response.body);
    List<dynamic> cards = json["cards"];
    print(json);
    cards.forEach((card) {
      Pokemon pokemon = Pokemon.fromJSON(card);
      pokemons.add(pokemon);
    });
    return pokemons;
  }
}
